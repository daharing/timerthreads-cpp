#include "threadTimers.h"
#include <stdio.h>
#include <mutex>



using namespace std;


/** the TestClass is a class designed to both test and demonstrate
 * how to use the thread timers. You can inherit or instantiate 
 * the ThreadTimers class but you must inherit the TimerCallback
 * class. The TimerCallback class contains an abstract function 
 * then you must then define and declare in your class.
 * 
 * This class works by implementing a thread safe timerCallback function
 * (from the TimerCallback class) and */
class TestClass : public TimerCallback, public ThreadTimers
{
  public:
    TestClass(){}
    ~TestClass(){}

  void timerCallback(unsigned int threadID, TimerDataStruct* data){
    accessControl.lock();
    unsigned int tlong = loopedTimerLong, tshort = loopedTimerShort;
     
    accessControl.unlock();
    if (data != NULL)
      printf("Timer %d \"%s\" has timed out!\n", threadID, (data->timerType == TIMERS_STRUCT::TIMER_BlinkTextLong)? "Long Timer" : "Short Timer");
    else
      printf("Timer %d has timed out!\n", threadID);

    if (threadID == tlong)
    {
      tlong = createTimer(TIMERS_STRUCT::_TIMERSENUM::TIMER_BlinkTextLong, this, data);
     printf("Create long timer\n");
    }
    else if (threadID == tshort)
    {
     printf("Create short timer\n");
      tshort = createTimer(TIMERS_STRUCT::_TIMERSENUM::TIMER_BlinkTextShort, this, data);
    }
    accessControl.lock();
    loopedTimerLong = tlong;
    loopedTimerShort = tshort;
    accessControl.unlock();
    
  }

  void start()
  {
    TimerDataStruct data, data2;
    data.timerType = TIMERS_STRUCT::TIMER_BlinkTextLong;
    data2.timerType = TIMERS_STRUCT::TIMER_BlinkTextShort;

    printf("Create initial timers\n");
    loopedTimerLong = createTimer(Ttimers.TIMER_BlinkTextLong, this, &data);

    loopedTimerShort = createTimer(Ttimers.TIMER_BlinkTextShort, this, &data2);
    
    while(1)
    {
      //loop forever to keep threads from dying

    }
  }

  private:
  TIMERS_STRUCT Ttimers;
  unsigned int loopedTimerLong, loopedTimerShort;
  mutex accessControl;

};



  int main()
  {
    TestClass test;
    test.start();

    return 0;
  }
