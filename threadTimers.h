/*
 * Author:  Dustin Haring
 * Date:    May 23, 2018
 * Comment: This file contains the declarations and definitions for 
 *          the different thread timers available 
 * 
 * Modified By: n/a
 * Date:        n/a
 * Comment:     n/a
 */
#ifndef THREADTIMERS_H
#define THREADTIMERS_H

#include <stdio.h>
#include <thread>
#include <mutex>
#include <unistd.h> //sleep
#include <map>
#include <assert.h>

/** INSTRUCTIONS*****
 * 
 * Modify the TIMERS_STRUCT to meet your needs. 
 * That is where you name the timers and set their
 * timeout values.
 * 
 * Modify the TimerDataStruct to have any data you
 * want the timer to pass to the timerCallback function.
 * 
 * Inherit the TimerCallback class and implement the 
 * timerCallback function. Be sure to make the callback
 * function 100% thread safe or you will get seg faults.
 * 
 * Create an instance of the ThreadTimer class and call
 * createTimer to create a timer.
 * 
 * */

/** 
 * Modify this struct to create the timers you desire.
 * Do this by creating the name of the timers in the
 * _TIMERSENUM enum and setting each timers corresponding
 * time in milliseconds in the constructor.
 * Must create an instance of this struct and the timers class.
 */
struct TIMERS_STRUCT
{
    enum _TIMERSENUM
    {
        TIMER_BlinkTextLong = 0,
        TIMER_BlinkTextShort,

        MAX_TIMERS
    };

    TIMERS_STRUCT() //set the timer values in ms
    {
        timerValues[TIMER_BlinkTextLong] = 1000;
        timerValues[TIMER_BlinkTextShort] = 50;
    }

    unsigned int getTime(TIMERS_STRUCT::_TIMERSENUM timerID)
    {
        if (timerID >= MAX_TIMERS || timerID < 0)
            assert(0);
        return timerValues[timerID];
    }

  private:
    unsigned int timerValues[MAX_TIMERS] = {0};
};

/** define all parameters you want the
 * timer thread to pass back to the 
 * callback function. */
struct TimerDataStruct
{
    TIMERS_STRUCT::_TIMERSENUM timerType;
};

/** inherit this class and implement the
 * callback function. This function is where
 * all the timer threads go when timed out. */
class TimerCallback
{
  public:
    virtual void timerCallback(unsigned int threadID, TimerDataStruct *data) = 0;
};

class ThreadTimers
{
  public:
    ThreadTimers() {
        //activeThread = new bool[std::thread::hardware_concurrency()] {false}; dah-0D limit threads to max number supported by system
    }
    ~ThreadTimers() {}

    unsigned int createTimer(TIMERS_STRUCT::_TIMERSENUM timerType, TimerCallback *cb, TimerDataStruct *data = NULL)
    {
        accessControlTimers.lock();
        unsigned int ms = timers.getTime(timerType);
        accessControlTimers.unlock();

        return createTimer(ms, cb, data);
    }
    unsigned int createTimer(unsigned int time_ms, TimerCallback *cb, TimerDataStruct *data = NULL)
    {
        std::thread *myThread;

        accessControlCount.lock();
        unsigned int lcount = count;
        accessControlCount.unlock();

        lcount++;

        accessControlCount.lock();
        accessControlMap.lock();
        // printf("Searching for availabled thread ID\n");
        while (activeThreads.count(lcount) != 0)
        {
            lcount++; //loop through ids until we find an ID that isn't in use
            // printf("search for new valid ID %d\n", lcount);
        }
            // printf("createTimer new valid ID is %d\n", lcount);
        accessControlMap.unlock();

        count = lcount;
        accessControlCount.unlock();

        myThread = new std::thread(&ThreadTimers::timerThread, this, time_ms, cb, this, lcount, data);

        myThread->detach(); //start thread
        myThread = NULL;
        return lcount;
    }

    void ignoreTimer(unsigned int threadID)
    {
        accessControlMap.lock();
        activeThreads.erase(threadID);
        accessControlMap.unlock();
    }

    // void killTimer(unsigned int threadID)
    // {
    //
    // }

  private:
    unsigned int count = 0;
    std::map<unsigned int, std::thread::id> activeThreads;
    TIMERS_STRUCT timers;
    std::mutex accessControlMap;
    std::mutex accessControlCount;
    std::mutex accessControlTimers;

    bool *activeThread;//bool array to keep track of thread ids

    void timerThreadCallback(unsigned int threadID, TimerCallback *cb, TimerDataStruct* data)
    {
        accessControlMap.lock();
        int eraseCount = activeThreads.erase(threadID);
        accessControlMap.unlock();

        if (eraseCount != 1)
        {
            assert(0);
        }
        else
        {
            cb->timerCallback(threadID, data);
        }
    }

    void timerThread(unsigned int x, TimerCallback *tc, ThreadTimers *ttcb, unsigned int threadID, TimerDataStruct *data)
    {
        accessControlMap.lock();
        activeThreads.insert(std::pair<unsigned int, std::thread::id>(threadID, std::this_thread::get_id()));
        accessControlMap.unlock();

        if (x == 0)
        {
            return;
        }
        else if (x < 1000)
        {
            usleep(x * 1000);
        }
        else
        {
            sleep(x / 1000);
        }
        ttcb->timerThreadCallback(threadID, tc, data);
    }
};

#endif
